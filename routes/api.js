const api = require("express").Router();
const AuthController = require("../controllers/api/authContoller");
const UserController = require("../controllers/api/userController");
const authApiContoller = new AuthController();
const userApiController = new UserController();
const bodyParser = require("body-parser");
const Restrict = require("../middlewares/restrict-api");

api.use(bodyParser.json());

// User Authentication
api.post("/user/register", authApiContoller.register);
api.post("/user/login", authApiContoller.login);
api.get("/user/logout", authApiContoller.logout);
api.delete("/user/:id", authApiContoller.delete);
api.use(Restrict);

// whoamiApi
api.get("/whoamiApi", authApiContoller.whoamiApi);

// Get User
api.get("/user", userApiController.getUser);
api.get("/user/:id", userApiController.getDetailUser);

// User Score
api.post("/insertScore", userApiController.insertScore);
api.get("/score", userApiController.getScore);
api.delete("/score/:id", userApiController.deleteScore);

// CREATE ROOM
api.post("/room", userApiController.createRoom);
// FIGHT ROOM
api.post("/fight/:room", userApiController.fightRoom);

module.exports = api;
