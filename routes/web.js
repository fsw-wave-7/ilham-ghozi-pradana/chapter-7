const web = require("express").Router();
const AuthController = require("../controllers/web/authController");
const authController = new AuthController();
const PageController = require("../controllers/web/pageController");
const pageController = new PageController();

web.get("/login", pageController.login);
web.post("/login", authController.logging);
web.get("/register", pageController.register);
web.post("/register", authController.registering);

// Dashboard Page
web.get("/", pageController.dashboard);
// User Lists
web.get("/users", pageController.userList);
// Room List
web.get("/room", pageController.room);
// Biodata List
web.get("/biodata", pageController.biodata);
module.exports = web;
