const createError = require("http-errors");
const express = require("express");
const path = require("path");
const app = express();
const logger = require("morgan");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const flash = require("express-flash");
const port = 3000;

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(
	session({
		secret: "rahasia",
		resave: false,
		saveUninitialized: false,
	})
);
// Passport JWT
const passportJwt = require("./lib/passport-jwt");
app.use(passportJwt.initialize());

app.use(flash());

// Passport Local
const passport = require("./lib/passport");
app.use(passport.initialize());
app.use(passport.session());

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// API ROUTE
const api = require("./routes/api");
app.use("/api", api);

// WEB ROUTE
const web = require("./routes/web");
app.use("/", web);

// catch 404 and forward to error handler
app.use((req, res, next) => {
	next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render("error");
});

app.listen(port, console.log(`server is running on ${port}`));

module.exports = app;
