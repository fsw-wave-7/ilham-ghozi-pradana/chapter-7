"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class History extends Model {
		
		static associate(models) {
			History.belongsTo(models.User, {
				foreignKey: "UserId",
				as: "userId",
			});
		}
	}
	History.init(
		{
			user_id: DataTypes.INTEGER,
			room_id: DataTypes.INTEGER,
			score: DataTypes.INTEGER,
			waktu_bermain: DataTypes.STRING,
			UserId: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "History",
		}
	);
	return History;
};
