"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class Biodata extends Model {
		
		static associate(models) {
		}
	}
	Biodata.init(
		{
			name: DataTypes.STRING,
			age: DataTypes.INTEGER,
			user_id: DataTypes.INTEGER,
      
		},
		{
			sequelize,
			modelName: "Biodata",
		}
	);
	return Biodata;
};
