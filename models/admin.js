"use strict";
const bcrypt = require("bcrypt");

const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class Admin extends Model {
		
		static associate(models) {
		}

		static register = ({username, password}) => {
			const encryptedPassword = bcrypt.hashSync(password, 10);
			return this.create({username, password: encryptedPassword});
		};

		checkPassword = (password) => bcrypt.compareSync(password, this.password);

		static authenticate = async ({username, password}) => {
			try {
				const admin = await this.findOne({where: {username}});
				if (!admin) return Promise.reject("User not found!");
				const isPasswordValid = admin.checkPassword(password);
				if (!isPasswordValid) return Promise.reject("Wrong password");
				return Promise.resolve(admin);
			} catch (err) {
				return Promise.reject(err);
			}
		};
	}
	Admin.init(
		{
			username: DataTypes.STRING,
			password: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "Admin",
		}
	);
	return Admin;
};
