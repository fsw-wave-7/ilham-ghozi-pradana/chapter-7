const {Admin, User, History, Biodata} = require("../../models");

class PageController {
	register = (req, res) => {
		res.render("index", {
			message: "register.",
			content: "pages/register",
		});
	};

	login = (req, res) => {
		res.render("index", {
			message: "hello.",
			content: "pages/login",
		});
	};

	dashboard = (req, res) => {
		Admin.findAll().then((admin) => {
			res.render("index", {
				message: "Admin List",
				content: "pages/tables/adminList",
				admin,
			});
		});
	};

	userList = (req, res) => {
		User.findAll().then((user) => {
			res.render("index", {
				message: "User List",
				content: "pages/tables/userList",
				user,
			});
		});
	};

	room = (req, res) => {
		History.findAll().then((room) => {
			res.render("index", {
				message: "Room List",
				content: "pages/tables/roomList",
				room,
			});
		});
	};

	biodata = (req, res) => {
		Biodata.findAll().then((biodata) => {
			res.render("index", {
				message: "Biodata List",
				content: "pages/tables/biodataList",
				biodata,
			});
		});
	};
}

module.exports = PageController;
